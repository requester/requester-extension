import { injectScript } from 'extension-observer/utils';
import { ChromePortStream } from 'extension-observer/stream';
import { MessageObserver, PostMessageStream } from 'requester-utils/protocol.fe';

injectScript('src/inject.js').then(() => {
    const observer = MessageObserver.from(PostMessageStream);
    observer.subscribe('fetch', async params => {
        return await MessageObserver.from(new ChromePortStream(chrome.runtime.connect())).publish('fetch', params);
    });
});