import { MessageObserver, PostMessageStream } from 'requester-utils/protocol.fe';

window.Extension = {
    async fetch(url, params) {
        return MessageObserver.from(PostMessageStream).publish('fetch', { url, params });
    }
};