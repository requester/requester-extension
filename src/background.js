import { ChromePortStream } from 'extension-observer/stream';
import { MessageObserver } from 'requester-utils/protocol.fe';

chrome.runtime.onConnect.addListener((port) => {
    const observer = MessageObserver.from(new ChromePortStream(port));
    observer.subscribe('fetch', async ({ url, params }) => {
        return await fetch(url, params);
    });
});