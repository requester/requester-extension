const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: "production",
    entry: {
        background: './src/background.js',
        content: './src/content.js',
        inject: './src/inject.js'
    },
    experiments: {
        outputModule: true,
    },
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                { from: path.resolve(__dirname, './images'), to: path.resolve(__dirname, './dist/images') },
                { from: path.resolve(__dirname, './manifest.json'), to: path.resolve(__dirname, './dist/manifest.json') }
            ]
        })
    ],
    output: {
        libraryTarget: "module",
        filename: '[name].js',
        path: path.resolve(__dirname, "./dist/src")
    }
}